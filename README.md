# README #

This repository contains an example web application that is a front end to a historical foreign exchange rate REST service (http://fixer.io). 
It is packaged as self-contained Maven project that builds into a Spring Boot application.

#### Pre-requisites
* Java 8
* Maven 3.x
* Git 2.x

#### Build it

1. `git clone git@bitbucket.org:vfunshteyn/fixer-proxy.git`
2. `mvn install`

#### Run it

`mvn spring-boot:run`

This will start the service inside an embedded Tomcat container on port 8080.

#### Test it

Unit tests are run as part of the build. For end to end testing, visit http://localhost:8080/currency-converter. This opens a simple data entry form that allows the user to specify the base, target currencies, amount in base currency to convert and point in time where to find rates. The only required input is the effective date radio button.

The service produces results suitable for consumption by any JSON-enabled client.
 
##### Error Handling
 
Spring Boot maps all service exceptions (including those returned by Fixer) to a default error page that produces a semantically correct status code.