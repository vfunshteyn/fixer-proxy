package net.mrvf.service;

import net.mrvf.Config;
import net.mrvf.domain.CurrencyRates;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = Config.class)
public class FixerClientTest extends AbstractJUnit4SpringContextTests {

    private FixerClient client;

    @Mock
    private RestTemplate template;

    @Mock
    private RestTemplateBuilder templateBuilder;

    @Autowired
    private DateTimeFormatter dateFormatter;

    @Before
    public void init() {
        when(templateBuilder.rootUri(any())).thenReturn(templateBuilder);
        when(templateBuilder.build()).thenReturn(template);
        client = new FixerClient("foo", templateBuilder, dateFormatter);
    }

    @Rule
    public final MockitoRule rule =
            MockitoJUnit.rule().strictness(Strictness.WARN);

    @Test
    public void getLatest() {
        client.getRatesFor(null, null);
        verify(template).getForObject("/latest", CurrencyRates.class);
    }

    @Test
    public void getLatestNonDefaultBase() {
        String base = "GBP";
        client.getRatesFor(null, base);
        verify(template).getForObject("/latest?base=" + base, CurrencyRates.class);
    }

    @Test
    public void getLatestNonDefaultBaseSpecificTerms() {
        String base = "GBP";
        String[] terms = {"AUD", "USD"};
        client.getRatesFor(null, base, terms);
        verify(template).getForObject("/latest?base=" + base + "&symbols=" +
                String.join(",", terms), CurrencyRates.class);
    }

    @Test
    public void getLatestSpecificTerms() {
        String[] terms = {"AUD", "USD"};
        client.getRatesFor(null, null, terms);
        verify(template).getForObject("/latest?symbols=" +
                String.join(",", terms), CurrencyRates.class);
    }

    @Test
    public void getSpecificDate() {
        DateTime date = dateFormatter.parseDateTime("2015-03-20");
        client.getRatesFor(date, null);
        verify(template).getForObject("/" + dateFormatter.print(date), CurrencyRates.class);
    }

    @Test
    public void getSpecificDateSpecificBaseDefaultTerms() {
        DateTime date = dateFormatter.parseDateTime("2015-03-20");
        String base = "GBP";
        client.getRatesFor(date, base);
        verify(template).getForObject("/" + dateFormatter.print(date) + "?base=" + base, CurrencyRates.class);
    }

    @Test
    public void getSpecificDateSpecificBaseSpecificTerms() {
        DateTime date = dateFormatter.parseDateTime("2015-03-20");
        String base = "GBP";
        String[] terms = {"AUD", "USD"};

        client.getRatesFor(date, base, terms);
        verify(template).getForObject("/" + dateFormatter.print(date) + "?base=" + base
                + "&symbols=" + String.join(",", terms), CurrencyRates.class);
    }

    @Test
    public void getSpecificDateDefaultBaseSpecificTerms() throws Exception {
        DateTime date = dateFormatter.parseDateTime("2015-03-20");
        String[] terms = {"AUD", "USD"};

        client.getRatesFor(date, null, terms);
        verify(template).getForObject("/" + dateFormatter.print(date) +
                "?symbols=" + String.join(",", terms), CurrencyRates.class);

    }

}