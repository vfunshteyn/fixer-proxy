package net.mrvf.model;

import java.math.BigDecimal;

public class RequestPayload {

    private String baseCcy;
    private String termCurrencies;
    private RateHistory date;

    private BigDecimal baseAmount;

    public String getBaseCcy() {
        return baseCcy;
    }

    public void setBaseCcy(String baseCcy) {
        this.baseCcy = baseCcy;
    }

    public String getTermCurrencies() {
        return termCurrencies;
    }

    public void setTermCurrencies(String termCurrencies) {
        this.termCurrencies = termCurrencies;
    }

    public RateHistory getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = RateHistory.fromId(date);
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RequestPayload{");
        sb.append("baseCcy='").append(baseCcy).append('\'');
        sb.append(", termCurrencies='").append(termCurrencies).append('\'');
        sb.append(", date='").append(date).append('\'');
        sb.append(", baseAmount=").append(baseAmount);
        sb.append('}');
        return sb.toString();
    }
}
