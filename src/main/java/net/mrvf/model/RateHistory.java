package net.mrvf.model;

import org.joda.time.DateTime;

import java.util.function.Supplier;

public enum RateHistory {
    NOW("now", () -> null), // use latest available rate
    ONE_YEAR_AGO("1yr", () -> DateTime.now().minusYears(1) ),
    FIVE_YEARS_AGO("5yrs", () -> DateTime.now().minusYears(5)),
    TEN_YEARS_AGO("10yrs", () -> DateTime.now().minusYears(10));

    RateHistory(String id, Supplier<DateTime> rewindBy) {
        this.id = id;
        this.rollBackOp = rewindBy;
    }
    private final String id;
    private final Supplier<DateTime> rollBackOp;

    static RateHistory fromId(String id) {
        for (RateHistory r: values()) {
            if (r.id.equals(id)) return r;
        }
        throw new IllegalArgumentException("Unknown historical point: " + id);
    }

    public Supplier<DateTime> getRollBackOp() {
        return rollBackOp;
    }
}
