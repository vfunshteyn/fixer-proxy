package net.mrvf.web;

import net.mrvf.domain.CurrencyRates;
import net.mrvf.model.RequestPayload;
import net.mrvf.service.FixerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ServiceController {

    @Autowired
    private FixerClient rateRetriever;

    @GetMapping("/currency-converter")
    public String renderInputForm(Model model) {
        model.addAttribute("currencyRates", new RequestPayload());
        return "search-form";
    }

    @PostMapping(path = "/result", produces = "application/json")
    @ResponseBody
    public CurrencyRates getRates(@ModelAttribute RequestPayload input) {
        String[] targetCcys = input.getTermCurrencies() == null ? null :
                input.getTermCurrencies().split("\\s*,\\s*");
        CurrencyRates rates = rateRetriever.getRatesFor(input.getDate().getRollBackOp().get(), input.getBaseCcy(), targetCcys);
        if (input.getBaseAmount() != null) {
            rates.convertAmount(input.getBaseAmount());
        }
        return rates;
    }

    @InitBinder     /* Converts empty strings into null when a form is submitted */
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }
}
