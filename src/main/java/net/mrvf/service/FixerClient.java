package net.mrvf.service;

import net.mrvf.domain.CurrencyRates;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class FixerClient {
    private final RestTemplate restTemplate;

    private final DateTimeFormatter dateFormatter;

    @Autowired
    public FixerClient(@Value("${fixerURI:http://api.fixer.io}") String baseUri, RestTemplateBuilder builder,
                       DateTimeFormatter formatter) {
        restTemplate = builder.rootUri(baseUri).build();
        dateFormatter = formatter;
    }

    public CurrencyRates getRatesFor(DateTime date, String base, String... terms) {
        String when = date == null ? "latest" : dateFormatter.print(date);

        StringBuilder uri = new StringBuilder();
        uri.append('/').append(when);

        if (base != null || (terms != null && terms.length > 0)) {
            uri.append('?');
            if (base != null) {
                uri.append("base=").append(base);
                if (terms != null && terms.length > 0) uri.append("&symbols=").append(String.join(",", terms));
            }
            else uri.append("symbols=").append(String.join(",", terms));
        }

        return restTemplate.getForObject(uri.toString(), CurrencyRates.class);
    }
}
