package net.mrvf.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class CurrencyRates {
    private final DateTime date;

    private final String base;

    private final Map<String, BigDecimal> rates;

    private final Map<String, BigDecimal> convertedAmounts;

    @JsonCreator
    public CurrencyRates(@JsonProperty("date") DateTime date,
                         @JsonProperty("base") String base,
                         @JsonProperty("rates") Map<String, BigDecimal> rates) {
        this.date = date;
        this.base = base;
        this.rates = rates;
        this.convertedAmounts = new HashMap<>(rates.size());
    }

    public DateTime getDate() {
        return date;
    }

    public String getBase() {
        return base;
    }

    public Map<String, BigDecimal> getRates() {
        return rates;
    }

    public Map<String, BigDecimal> getConvertedAmounts() {
        return convertedAmounts;
    }

    public void convertAmount(BigDecimal amount) {
        rates.entrySet().forEach(r ->
                convertedAmounts.put(r.getKey(), r.getValue().multiply(amount))
        );
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CurrencyRates{");
        sb.append("date=").append(date);
        sb.append(", base='").append(base).append('\'');
        sb.append(", rates=").append(rates);
        sb.append('}');
        return sb.toString();
    }
}
