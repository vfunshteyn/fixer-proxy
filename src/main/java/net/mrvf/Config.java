package net.mrvf;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.joda.cfg.JacksonJodaDateFormat;
import com.fasterxml.jackson.datatype.joda.deser.DateTimeDeserializer;
import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;

import javax.annotation.PostConstruct;

@Configuration
public class Config {

    @Autowired
    private ConfigurableEnvironment env;

    @PostConstruct
    public void init() {
        MapPropertySource ps = (MapPropertySource) env.getPropertySources().get("systemProperties");
        ps.getSource().put("logging.level.org.springframework.web.client", "DEBUG");
    }

    @Bean
    public ObjectMapper jacksonObjectMapper(DateTimeFormatter dateTimeFormatter) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        JodaModule jodaModule = new JodaModule();
        JacksonJodaDateFormat dtFmt = new JacksonJodaDateFormat(dateTimeFormatter);
        DateTimeSerializer dtSer = new DateTimeSerializer(dtFmt);
        JsonDeserializer dtDeser = new DateTimeDeserializer(DateTime.class, dtFmt);
        jodaModule.addSerializer(dtSer);
        jodaModule.addDeserializer(DateTime.class, dtDeser);

        mapper.registerModule(jodaModule);
        mapper.registerModule(new ParameterNamesModule());
        return mapper;
    }

    @Bean
    public DateTimeFormatter dateFormatter() {
        return DateTimeFormat.forPattern("yyyy-MM-dd");
    }
}
